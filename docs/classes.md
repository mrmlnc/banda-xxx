## Классы

В этом документе описаны все классы, доступные для оформления текста, кнопок и модальных окон.




### Вспомогательные классы

#### Цвета

```css
.-c-grey-300 { color: #6d7178 }
.-c-grey-700 { color: #353b43 }
.-c-red { color: #f00; }
.-c-green { color: #86b022; }
.-c-blue { color: #009be1; }
```


#### Управление потоком (отступы)

```css
.-mb-0 { margin-bottom: 0; }

.-mb-em-0-5 { margin-bottom: .5em; }
.-mb-em-1 { margin-bottom: 1em; }
.-mb-em-1-5 { margin-bottom: 1.5em; }
.-mb-em-2 { margin-bottom: 2em; }
.-mb-em-3 { margin-bottom: 3em; }

.-mb-px-10 { margin-bottom: 10px; }
.-mb-px-15 { margin-bottom: 15px; }
.-mb-px-20 { margin-bottom: 20px; }
.-mb-px-30 { margin-bottom: 30px; }
.-mb-px-40 { margin-bottom: 40px; }
.-mb-px-50 { margin-bottom: 50px; }
```

#### Управление шрифтами

```css
/* Толщина шрифта */
.-fw-light { font-weight: 300 !important; }
.-fw-regular { font-weight: 400 !important; }
.-fw-semibold { font-weight: 600 !important; }
.-fw-bold { font-weight: 700 !important; }

/* Размер шрифта */
.-fs-xsmall { font-size: 1rem !important; }
.-fs-small { font-size: 1.2rem !important; }
.-fs-regular { font-size: 1.4rem !important; }
.-fs-middle { font-size: 1.7rem !important; }
.-fs-large { font-size: 1.8rem !important; }
.-fs-xlarge { font-size: 2rem !important; }
.-fs-xxlarge { font-size: 2.4rem !important; }

/* Межстрочный интервал */
.-lh-1 { line-height: 1 !important; }
.-lh-1-25 { line-height: 1.25 !important; }
.-lh-1-5 { line-height: 1.5 !important; }
.-lh-1-75 { line-height: 1.75 !important; }
.-lh-2 { line-height: 2 !important; }

/* Прочее */
.-text-center {
  margin-right: auto;
  margin-left: auto;
  text-align: center !important;
}
```




### Кнопки

#### Разновидность кнопок

```css
/* Синяя кнопка */
.btn.btn-blue

/* Зелёная кнопка */
.btn.btn-green

/* Жёлтая кнопка */
.btn.btn-yellow

/* Прозрачная кнопка */
.btn.btn-transparent

/* Синяя кнопка без фона, но в синей рамке */
.btn.btn-outline-blue

/* Чёрная кнопка без фона, но в черной рамке */
.btn.btn-outline-black
```


#### Кнопка помощи

```html
<button class="btn btn-help" data-balloon="Текст подсказки" data-balloon-pos="up" data-balloon-promo="">?</button>
```


#### Кнопка покупки

```html
<a href="#buy" title="title" data-collapse="" data-target="#buyButton" id="buyButton" class="btn btn-yellow btn-buy-with-spinner">
  <span class="buy">Купить</span>
  <span class="spinner"></span>
  <span class="cart">В корзине</span>
</a>
```




### Таблицы

#### Синяя таблица

```html
<table class="table -blue">
  ...
</table>
```



### Лист

#### Список без маркировок

```html
<ul class="list-unstyled">
  ...
</ul>
```




### Модальное окно

В точности повторяет классы из Bootstrap.
