## Информация для backend-разработчика

Все вопросы вы можете задавать по адресу: dmalinochkin@rambler.ru (даже, если вы хотите пожелать мне зла)



### Ссылки

Описание директорий шаблонов: https://www.canonium.com/articles/organizing-html-code-in-project
Описание директорий стилей: https://canonium.com/articles/organizing-code-in-project
Методология CSS: http://rscss.io/



### Общие положения

Некоторые страницы имеют класс на тэге `body`. Вот их список в соответствии с именами html-файлов:

  * 404
  * buy
  * cart
  * catalog
  * delivery
  * index (home)
  * order
  * partners
  * payment
  * product
  * product
  * reviews
  * shipping
  * vacancies
  * vacancy

> За связано с тем, что компоненты (блоки) меняют своё оформление от страницы к странице и наличием правок по выравниванию блоков.

В соответствии с этим, в body для этих страниц необходимо указать класс: `page-ИМЯ_СТРАНИЦЫ`.



### Оформление текста

Чтобы включить оформление содержимого (списки с кружочками и прочее) для текста на странице, например, "о банде" или "доставка и гарантия",
необходимо вложить контент в тег с классом `page-content`.



### Оформление страниц

Одноколоночный макет:

```html
<div class="page-main -one">
  ...
</div>
```

Двуколочный макет:

```html
<div class="site-main">
  <div class="page-main">...</div>
  <div class="page-sidebar">...</div>
</div>
```



### Связка кнопки покупки продукта и действий с неё

Для увеличения производительности фронтенда - поведение кнопок выполнено классами. 

```html
<a href="#buy" title="title" data-collapse="" data-target="#buyButton" id="buyButton" class="btn btn-yellow btn-buy-with-spinner">
  <span class="buy">Купить</span>
  <span class="spinner"></span>
  <span class="cart">В корзине</span>
</a>
```

Соответственно у каждой кнопки должен быть задан идентификатор, например, `buyButton-0` или `buyButton-ИМЯ_ПРОДУКТА`. Идентификатор должен быть
отражён в атрибуте `data-target` у этой кнопки.

Например, так (без классов для примера):
```html
<button data-collapse="" data-target="#buyButton-0" id="buyButton-0"></button>
<button data-collapse="" data-target="#buyButton-1" id="buyButton-1"></button>
<button data-collapse="" data-target="#buyButton-2" id="buyButton-2"></button>
```




### Скрытие и показ блоков

Для отображения блоков или их скрытия необходимо воспользоваться модулем `collapse`. Настройка осуществляется через атрибуты:

  * data-no-scroll -- запретить скролл
  * data-scroll-top -- перейти к верху страницы
  * data-hidden-class -- переключить указанный класс у целевого элемента
  * data-hide -- скрыть элемент или массив элементов
  * data-only-add -- только добавление класса показа элемента (без скрытия)
  * data-no-prevent -- включить действие по умолчанию




### Настройка слайдеров

Вся настройка слайдеров находится в модуле `pgw` и `slider`. Первый файл отвечает за настройку слайдера PGW на главной странице. Второй, за все остальные.




### Настройка табов

Вся настройка табов находится в модуле `tabs`. Для того, чтобы создать табы, необходимо:

  * Добавить родительскому элементу атрибут `data-tabs-controller`
  * Создать классы `.tabs-heading` и `.tabs-content`
  * Добавить атрибуты `data-tab`, значение которых совпадают в заголовке и содержимом табов

Пример: 
  
```html
<div data-tabs-controller="data-tabs-controller" class="c-tabs (ДОП. КЛАССЫ СЮДА)">
  <div class="tabs-navbar">
    <ul class="tabs-navigation">
      <li><a data-tab="orders" href="#0" class="-active">Мои заказы</a></li>
      <li><a data-tab="profile" href="#1">Профайл</a></li>
    </ul>
  </div>
  
  <ul class="tabs-content">
    <li data-tab="orders" class="tab-content -active">
      Содержимое таба
    </li>
    <li data-tab="profile" class="tab-content">
      Содержимое таба
    </li>
  </ul>
</div>
```

Дополнительные классы:

  * `.-full` - развернуть на всю страницу в мобильной версии
  * `.-without-first` - скрыть первый таб (используется на странице продукта)
  * `.-center` - выравнивание элементов управления по центру
  * `.-square` - квадратные элементы управления (используется на странице "где купить")
  
