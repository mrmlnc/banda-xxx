'use strict';

const $ = use(
  'chalk',
  'slash',
  'gulp-sourcemaps',
  'gulp-sass',
  'gulp-sass-glob',
  'gulp-postcss',
  'autoprefixer',
  'css-mqpacker',
  'postcss-flexbugs-fixes'
);

const autoprefixerConfig = [
  // Microsoft
  'Explorer >= 10',
  'Edge >= 12',
  'ExplorerMobile >= 10',
  // Mozilla
  'Firefox >= 30',
  // Google
  'Chrome >= 34',
  'Android >= 4.4',
  // Opera
  'Opera >= 12',
  // Apple
  'Safari >= 7',
  'iOS >= 7',
  // BlackBerry
  'BlackBerry >= 10'
];

function sassErrorHandler(err) {
  err.message.split('\n').forEach((line) => {
    console.log($.chalk.red('>> ') + line);
  });

  this.emit('end');
}

function task() {
  return $.gulp.src('app/styles/scss/styles.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sassGlob())
    .pipe($.sass().on('error', sassErrorHandler))
    .pipe($.postcss([
      $.autoprefixer({ browsers: autoprefixerConfig }),
      $.postcssFlexbugsFixes(),
      $.cssMqpacker({
        sort: true
      })
    ]))
    .pipe($.sourcemaps.write('.'))
    .pipe($.gulp.dest('build/styles'));
}

module.exports = {
  task
};
