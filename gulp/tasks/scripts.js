'use strict';

const $ = use(
  'chalk',
  'slash',
  'rollup',
  'rollup-plugin-babel',
  'gulp-sourcemaps'
);

function rollupErrorHandler(err) {
  console.log($.chalk.red('>> ') + err);
}

function task() {
  const babelPlugin = $.rollupPluginBabel({
    babelrc: false,
    presets: [
      ['es2015', { modules: false }]
    ],
    plugins: ['external-helpers']
  });

  const options = {
    entry: './app/scripts/scripts.js',
    plugins: [babelPlugin]
  };

  return $.rollup.rollup(options).then((bundle) => {
    return bundle.write({
      format: 'iife',
      dest: 'build/scripts/scripts.bundle.js'
    });
  }).catch(rollupErrorHandler);
}

module.exports = {
  task
};
