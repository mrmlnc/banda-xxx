'use strict';

const $ = use('gulp.spritesmith');

function task() {
  const spriteData = $.gulp.src('app/images/sprites/**/*').pipe($.spritesmith({
    imgName: 'images/sprite.png',
    cssName: 'styles/sprite.css'
  }));
  return spriteData.pipe($.gulp.dest('build'));
}

module.exports = {
  task
};
