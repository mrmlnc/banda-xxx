'use strict';

const path = require('path');
const fs = require('fs');

const $ = use(
  'chalk',
  'slash',
  'yellfy-pug-inheritance',
  'gulp-filter',
  'gulp-jade'
);

// Cache for incremental rebuilds
let jsonDataCache = null;
let treeCache = {};

function getJsonData(dir) {
  const obj = {};
  const errors = [];

  fs.readdirSync(dir).forEach((filename) => {
    const basename = path.basename(filename, '.json');
    const filepath = path.join(dir, filename);

    try {
      const data = fs.readFileSync(filepath);
      obj[basename] = JSON.parse(data);
    } catch (err) {
      errors.push(filename);
    }
  });

  return errors.join(', ') || obj;
}

function jadeErrorHandler(err) {
  let msg = err.message.split('\n');
  msg[0] = err.name + ': ' + msg[0];
  msg.forEach((line) => {
    line = $.slash(line.replace(process.cwd() + '\\', ''));
    console.log($.chalk.red('>> ') + line);
  });
}

function jadeFilter(file, inheritance) {
  if (!global.watch) {
    return true;
  }

  const filepath = `app/templates/${file.relative}`;
  const needToUpdate = inheritance.checkDependency(filepath, global.changedTemplateFile);
  if (needToUpdate || path.extname(global.changedTemplateFile) === '.json') {
    console.log($.chalk.blue('>> ') + `Compiling: ${filepath}`)
    return true;
  }

  return false;
}

function task(done) {
  const changedFile = global.changedTemplateFile;
  if (!jsonDataCache || (changedFile && path.extname(changedFile) === '.json')) {
    jsonDataCache = getJsonData('app/templates/data');
  }

  if (typeof jsonDataCache !== 'object') {
    console.log($.chalk.red('>> ') + `JSON syntax error: ${jsonDataCache}`);
    return done();
  }

  return new Promise((resolve, reject) => {
    $.yellfyPugInheritance.updateTree('./app/templates', { changedFile, treeCache, jade: true }).then((inheritance) => {
      treeCache = inheritance.tree;

      $.gulp.src('app/templates/*.jade')
        .pipe($.filter((file) => jadeFilter(file, inheritance)))
        .pipe($.jade({ pretty: true, data: jsonDataCache }).on('error', (err) => {
          jadeErrorHandler(err);
          reject();
        }))
        .pipe($.gulp.dest('build'))
        .on('end', resolve)
        .on('error', reject);
    });
  });
}

module.exports = {
  task
};
