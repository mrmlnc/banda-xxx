'use strict';

const $ = use('browser-sync');

function task() {
  global.watch = true;

  $.browserSync({
    online: false,
    notify: false,
    logPrefix: 'Yellfy',
    server: ['build'],
    cors: true,
    port: 8000
  });

  // Directory synchronization
  $.gulp.watch([
    'app/fonts/**',
    'app/images/**/*.{gif,jpg,png,svg}',
    'app/{scripts,styles}/vendor/**',
    'app/*'
  ], $.gulp.series('sync', 'reload'));

  // Scripts
  $.gulp.watch([
    'app/scripts/modules/**/*.js',
    'app/scripts/scripts.js'
  ], $.gulp.series('xo', 'scripts', 'concat', 'reload'));

  // Styles
  $.gulp.watch(
    'app/styles/scss/**/*.scss',
    $.gulp.series('styles', 'reload')
  );

  // Sprites
  $.gulp.watch(
    'app/images/sprites/**/*.{gif,jpg,png,svg}',
    $.gulp.series('sprites', 'reload')
  );

  // Templates
  $.gulp.watch([
    'app/templates/**/*',
    'app/{scripts,styles}/inline/**'
  ], $.gulp.series('templates', 'reload'))
    .on('all', (event, filepath) => {
      global.changedTemplateFile = filepath.replace(/\\/g, '/');
    });

  // Bower
  $.gulp.watch(['bower.json'], $.gulp.series(
    $.gulp.parallel('sync-bower', 'templates'),
    'reload'
  ));
}

module.exports = {
  task
};
