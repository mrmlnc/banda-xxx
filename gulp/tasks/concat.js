'use strict';

const $ = use(
  'gulp-concat'
);

function task() {
  return $.gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'bower_components/jquery.steps/build/jquery.steps.min.js',
    'build/scripts/vendor/owl.carousel.min.js',
    'build/scripts/vendor/pgwslider.min.js',
    'build/scripts/vendor/jquery.scrolldetector.min.js',
    'build/scripts/vendor/bootstrap.modal.min.js',
    'build/scripts/vendor/awesomplete.min.js',
    'build/scripts/scripts.bundle.js'
  ])
    .pipe($.concat('scripts.bundle.js'))
    .pipe($.gulp.dest('build/scripts'));
}

module.exports = {
  task
};
