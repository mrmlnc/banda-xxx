'use strict';

import $ from 'jquery';

import _ from './_helpers';

const $wizard = $('#basketWizard');

let skipByStepWizard = false;

/**
 * Переключаем состояния корзины
 */
function skipStepWizard($wizard) {
  const url = decodeURIComponent(window.location);
  const isSkipStep = url.indexOf('skipStep=') !== -1;
  if (isSkipStep) {
    const skippedSteps = url.match(/skipStep=(\d)/)[1];
    if (skippedSteps === '1') {
      skipByStepWizard = true;
      $wizard.steps('next');
      skipByStepWizard = false;
    } else if (skippedSteps === '2') {
      skipByStepWizard = true;
      $wizard.steps('next');
      $wizard.steps('next');
      skipByStepWizard = false;
    }
  }
}

/**
 * Кастомный select
 */
function countrySelector() {
  const $countrySelector = $('#countrySelector');
  if (!$countrySelector) {
    return;
  }

  const awesomplete = new Awesomplete($countrySelector[0], {
    minChars: 0
  });

  $(awesomplete.container).on('awesomplete-selectcomplete', () => {
    const country = $countrySelector.val();

    $.ajax({
      url: window.baseUrl + '/basket/country',
      dataType: 'json',
      type: 'GET',
      data: {
        _csrf: $('meta[name="csrf-token"]').attr('content'),
        country
      }
    }).done((result) => {
      updateProductData(result);
    });
  });
}

/**
 * Обновление данных продукта:
 *   * Валидация
 *   * Стоимость
 *
 * @param {any} data Ответ от сервера
 * @param {any} $productContainer Контейнер продукта
 */
function updateProductData(data, $productContainer) {
  if (!data) {
    return;
  }

  // Обновление стоимости товара с учётом количества
  if ($productContainer) {
    $productContainer.find('.items-price').text(data.itemsCost);
  }

  // Обновление стоимости всех товаров
  $('.total-price').text(data.totalCost);

  // Обновление стоимости доставки
  $('.delivery-price').text(data.deliveryCost);

  // Вес посылки
  $('.total-weight').text(data.totalWeight);

  // Статус посылки
  if (data.isBigWeight) {
    $('#paymentList').removeClass('-show');
    $('#paymentManual').addClass('-show');
  } else {
    $('#paymentList').addClass('-show');
    $('#paymentManual').removeClass('-show');
  }

  // Если найдены невалидные товары
  if (data.notValidProducts) {
    const $basket = $('#basketMain');
    const $products = $basket.find('tr');

    $products.each((index, el) => {
      const $el = $(el);
      const id = parseInt($el.attr('data-product-id'), 10);

      // Убираем выделение для ныне валидных элементов
      if (data.notValidProducts.indexOf(id) === -1) {
        $el.removeClass('-invalid');
      } else {
        $el.addClass('-invalid');
      }
    });
  }

  _.drawHeaderBasket(data);
}

/**
 * Обработчик события изменения количества товаров
 *
 * @param {any} event Событие
 */
function changeQuantityHandler(event) {
  const $triggerEl = $(event.target);
  const $counterContainer = $triggerEl.closest('.counter');
  const $counterEl = $counterContainer.find('.current-count');

  const $productContainer = $triggerEl.closest('[data-product-id]');

  const triggerType = $triggerEl.hasClass('btn-step-up') ? 'up' : 'down';
  const productId = parseInt($productContainer.attr('data-product-id'), 10);
  let quantity = parseInt($counterEl.text(), 10);
  const country = $('#countrySelector').val();

  if (quantity !== 1 && triggerType === 'down') {
    quantity -= 1;
  }
  if (triggerType === 'up') {
    quantity += 1;
  }

  $.ajax({
    url: window.baseUrl + '/basket/quantity',
    type: 'GET',
    data: {
      productId,
      quantity,
      _csrf: $('meta[name="csrf-token"]').attr('content'),
      country
    }
  }).done((result) => {
    if (result.status === 'ok') {
      $counterEl.text(quantity);
      updateProductData(result, $productContainer);
    }
  });
}

/**
 * Обработчик события удаления продукта
 *
 * @param {any} event Событие
 */
function removeProductHandler(event) {
  const $triggerEl = $(event.target).closest('[data-product-id]');
  const productId = parseInt($triggerEl.attr('data-product-id'), 10);
  const country = $('#countrySelector').val();

  $.ajax({
    url: window.baseUrl + '/basket/delete',
    dataType: 'json',
    type: 'GET',
    data: {
      productId,
      _csrf: $('meta[name="csrf-token"]').attr('content'),
      country
    }
  }).done((result) => {
    if (result.status === 'ok') {
      $triggerEl.remove();
      updateProductData(result, null);
    }
  });
}

/**
 * Попытка перехода на следующий шаг корзины
 *
 * @param {any} event Событие
 * @param {any} currentIndex Текущий шаг
 * @returns
 */
function onStepChanging(event, currentIndex, nextIndex) {
  // Разрешаем идти назад, но не вперед
  if (currentIndex > nextIndex || skipByStepWizard) {
    return true;
  }

  return true;
}

/**
 * Проверка перед отправкой корзины
 */
function onFinishing() {
  return true;
}

/**
 * Отправка корзины
 */
function onFinished() {
  // Какие-то действия после полной валидации корзины, например, отправка на сервер

  console.log('~~FINISH~~');
}

/**
 * Инициализация корзины
 */
function init() {
  // Если на странице присутствует мастер корзины, то вешаем на него jquery.steps плагин
  if ($wizard[0]) {
    $wizard.steps({
      headerTag: '.tab-heading',
      bodyTag: '.tab-content',

      // Отключаем навигацию плагина
      enableKeyNavigation: false,
      enablePagination: false,

      // Вешаем обработчики событий шагов
      onStepChanging,
      onFinishing,
      onFinished,

      // Убиваем лишний текст
      labels: {
        current: ''
      }
    });
  } else {
    return;
  }

  // Регистрируем кастомный select
  countrySelector();

  // Регистрируем перехода по параметрам в URI
  skipStepWizard($wizard);

  // Попытка перейти на следующий шаг корзины
  $(document).on('click', '.btn-next-step', (event) => {
    if ($wizard) {
      event.preventDefault();
      $wizard.steps('next');
    }
  });
  $(document).on('click', '.btn-finish-step', (event) => {
    if ($wizard) {
      event.preventDefault();
      $wizard.steps('finish');
    }
  });
  $(document).on('click', '.btn-step-down, .btn-step-up', (event) => {
    if ($wizard) {
      event.preventDefault();
      changeQuantityHandler(event);
    }
  });
  $(document).on('click', '#basketMain [data-balloon-remove]', (event) => {
    if ($wizard) {
      event.preventDefault();
      removeProductHandler(event);
    }
  });
}

export default {
  init
};
