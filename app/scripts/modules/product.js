'use strict';

import $ from 'jquery';
import '../vendor/magnific.min';

function init() {
  const hash = window.location.hash;
  if (hash && hash.indexOf('#comments') !== -1) {
    $('.tabs-navigation a[data-tab="feedback"]').trigger('click');
  }

  const $productSlider = $('#productSlider');
  const $productSliderCurrent = $('#productSliderCurrent');
  $productSlider.owlCarousel({
    items: 1,
    navigation: false,
    pagination: false,
    lazyLoad: true,
    itemsCustom: [
      [0, 1],
      [768, 4]
    ]
    // autoPlay: true
  });

  $productSliderCurrent.html($productSlider.find('.item').eq(0).clone());
  $('.slider-list #productSlider').on('click', function(event) {
    const $item = $(event.target);
    if (!$item.hasClass('item')) {
      return;
    }
    $productSlider.find('.item').removeClass('-active');
    $item.addClass('-active');

    $productSliderCurrent.html($item.clone());
  });

  $('.product-slider-control.-prev').click(() => {
    $productSlider.trigger('owl.prev');
  });

  $('.product-slider-control.-next').click(() => {
    $productSlider.trigger('owl.next');
  });

  // Запуск popup-а для текущего слайда
  $productSliderCurrent.on('click', (event) => {
    event.preventDefault();

    const magnificPopupItems = [];
    $productSlider.find('.item').each((index, el) => {
      const imageUrl = el.style.backgroundImage.trim();
      magnificPopupItems.push({
        src: imageUrl.substring(5, imageUrl.length - 2)
      });
    });

    $.magnificPopup.open({
      type: 'image',
      items: magnificPopupItems,
      removalDelay: 300,
      mainClass: 'mfp-fade',
      closeMarkup: '',
      gallery: {
        enabled: true
      }
    });
  });
}

export default {
  init
};
