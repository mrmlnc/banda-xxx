'use strict';

import $ from 'jquery';
import _ from './_helpers';

/**
 * @param {jQuery} $this
 * @param {Object} data
 */
function alertBeforeFormHandler($this, data) {
  const $alert = $($.parseHTML(`<div class="c-alert">${data.message}</div>`));
  if (data.status === true) {
    $alert.addClass('-success');
  } else {
    $alert.addClass('-error');
  }

  $alert.insertBefore($this);
  $this.hide();
}

/**
 * @param {jQuery} $this
 * @param {Object} data
 */
function productFormHandler($this, data) {
  $this = $this.closest('.action-block');
  if (data.status === true) {
    $this.addClass('-success');
  } else {
    $this.addClass('-error');
  }
}

function init() {
  $('[data-form-sender]').on('submit', function(event) {
    event.preventDefault();

    const $this = $(this);
    const type = $this.attr('data-form-type');
    const pageUrl = window.location.href;
    // const formAction = $this.attr('action');

    const pizza = _.pizzaShow();
    pizza.show();

    $.ajax({
      type: 'GET',
      url: window.baseUrl + '/forms',
      // url: window.baseUrl + formAction,
      data: $this.serialize() + `&pageUrl=${pageUrl}`
    }).done((result) => {
      setTimeout(() => {
        if (type === 'alertBefore') {
          alertBeforeFormHandler($this, result);
        } else if (type.indexOf('product') === 0) {
          productFormHandler($this, result);
        }

        pizza.hide();
      }, 1500);
    });
  });
}

export default {
  init
};
