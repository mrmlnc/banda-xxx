'use strict';

import $ from 'jquery';

import _ from './_helpers';

function init() {
  $('[data-button-buy]').on('click', function() {
    const $this = $(this);
    const productId = $this.attr('data-product-id');

    $.ajax({
      url: window.baseUrl + '/basket/add',
      type: 'GET',
      data: {
        productId,
        _csrf: $('meta[name="csrf-token"]').attr('content')
      }
    }).done((result) => {
      if (result.status === 'ok') {
        $this.toggleClass('-on');
        _.drawHeaderBasket(result);
      }
    });
  });
}

export default {
  init
};
