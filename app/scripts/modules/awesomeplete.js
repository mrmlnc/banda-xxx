'use strict';

import $ from 'jquery';

function init() {
  $('.c-awesomplete').each((index, el) => {
    const $el = $(el);
    const listEl = $el.attr('data-json-list');
    let list;

    try {
      list = JSON.parse($(listEl)[0].innerText);
    } catch (err) {
      // silence
    }

    if (list) {
      /* eslint no-new: 0*/
      new Awesomplete(el, {
        list: list,
        replace: function(item) {
          this.input.value = item.label;
          this.input.setAttribute('data-country', item.value);
        }
      });
    }
  });
}

export default {
  init
};
