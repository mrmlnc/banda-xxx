'use strict';

import $ from 'jquery';
import _ from './_helpers';

function init() {
  if (!_.checkWindowWidth(768)) {
    const $fixedFilterButton = $('#fixedFilterButton');
    $(document).on('scrollUp', function() {
      $fixedFilterButton.removeClass('-out-down');
      $fixedFilterButton.addClass('-on -in-up');
    });
    $(document).on('scrollDown', function() {
      $fixedFilterButton.removeClass('-in-up');
      $fixedFilterButton.addClass('-out-down');
    });
  }
}

export default {
  init
};
