'use strict';

import $ from 'jquery';
import _ from './_helpers';

$(window).resize(() => {
  if (!window.requestAnimationFrame) {
    setTimeout(moveNavigation, 300);
  }

  window.requestAnimationFrame(moveNavigation);
});

// Открытие меню
function toggleMenu() {
  const $navbar = $('#navbar');
  const $navbarDropdowns = $('.navbar-dropdown');
  const $body = $('body');
  const $html = $('html');

  $body.toggleClass('-offset-navbar');
  $html.toggleClass('-scroll-off');
  $navbar.toggleClass('-hidden');

  $navbarDropdowns.each(function() {
    $(this).addClass('-hidden');
  });
}

// Переключение состояния меню
$(document).on('click', '[data-navbar-trigger]', function() {
  $(this).toggleClass('-active');
  toggleMenu();
});

// DEMO FOR SEARCH -- REMOVE ON PRODUCTION
$(document).on('click', '[data-search]', function() {
  $(this).toggleClass('-active');
  $('#searchResults').toggleClass('-on');
});

$(document).on('click', '[data-search-reset]', function() {
  $('#searchResults').removeClass('-on');
});
// --

// Обработка нажатия вне меню для его закрытия
$(document).on('click', function(event) {
  const isNavbarMain = $(event.target).closest('.c-navbar').length;
  const isNavbarWrapper = $(event.target).closest('.navbar-wrapper').length;
  const isTopbar = $(event.target).closest('.c-smallbar').length;
  // DEMO FOR SEARCH -- REMOVE ON PRODUCTION
  const isSearch = $(event.target).closest('.navbar-search').length;
  const isSearchResults = $(event.target).closest('.c-search').length;
  // --
  if (isNavbarMain || isNavbarWrapper || isTopbar || isSearch || isSearchResults) {
    return;
  }

  const $navbar = $('#navbar');
  if (!$navbar.hasClass('-hidden')) {
    $('[data-navbar-trigger]').toggleClass('-active');
    toggleMenu();
  }

  const $navbarDropdowns = $('.navbar-dropdown');
  $navbarDropdowns.each(function() {
    $(this).prev('a').removeClass('-active');
    $(this).addClass('-hidden');
  });

  const $search = $('#globalSearch');

  // DEMO FOR SEARCH -- REMOVE ON PRODUCTION
  const $searchResults = $('#searchResults');
  // --
  if ($search.hasClass('-on')) {
    $search.removeClass('-on');
    // DEMO FOR SEARCH -- REMOVE ON PRODUCTION
    $searchResults.removeClass('-on');
    // --
  }
});

// Переключение состояния меню второго уровня
$('.has-children').children('a').on('click', function(event) {
  const $this = $(this);
  const isDesktop = _.checkWindowWidth(768);
  if (!isDesktop) {
    event.preventDefault();
  }

  if (isDesktop && $this.parent('li').hasClass('only-mobile')) {
    return;
  }

  if ($this.next('ul')) {
    $('.navbar-wrapper').scrollTop(0);
    $this.toggleClass('-active');
    $this.next('ul').toggleClass('-hidden');

    let wrapperHeight = 0;
    const parentHeight = $this.parent('li').closest('ul.navbar-dropdown').height();
    $this.parent('li').closest('ul.navbar-dropdown').children('li').each(function() {
      wrapperHeight += $(this).outerHeight();
    });

    $this.next('ul').height((wrapperHeight > parentHeight) ? wrapperHeight : parentHeight);
  }
});

$(document).on('click', '.navbar-dropdown-back', function(event) {
  event.preventDefault();
  $(this).closest('ul').toggleClass('-hidden');
});

// Перемещение навигации
function moveNavigation() {
  const $navbar = $('#navbar');
  const $search = $('#globalSearch');
  const $searchResults = $('#searchResults');
  const isDesktop = _.checkWindowWidth(768);

  $navbar.detach();
  $search.detach();
  if (isDesktop) {
    $navbar.insertBefore('.c-navbar .navbar-search-trigger');
    $search.insertBefore('.c-navbar .navbar-search-trigger');
    $searchResults.insertBefore('.c-navbar .navbar-search-trigger');
  } else {
    $navbar.insertAfter('.site-main');
    $search.insertAfter('.site-main');
    $searchResults.insertAfter('.site-main');
  }
}

// Функция инициализации
function init() {
  moveNavigation();
}

export default {
  init
};
