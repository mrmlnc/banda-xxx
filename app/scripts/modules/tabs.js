'use strict';

import $ from 'jquery';
import _ from './_helpers';

function updateFirstTab(navbar, content) {
  if (_.checkWindowWidth(768)) {
    $('body').removeClass('-tab-game');
    navbar.find(`[data-tab="game"]`).removeClass('-active');

    if (!($('body').attr('class').indexOf('-tab-') + 1)) {
      navbar.find(`[data-tab="description"]`).addClass('-active');
      content.find(`[data-tab="description"]`).addClass('-active');
    }
  }
}

function init() {
  const $tabs = $('[data-tabs-controller]');
  const $allTabsNavbars = $tabs.find('.tabs-navigation');
  const $allTabsContent = $tabs.find('.tabs-content');

  updateFirstTab($allTabsNavbars, $allTabsContent);

  $allTabsNavbars.on('click', '[data-tab]:not(.-active)', function(event) {
    const $this = $(this);
    const $tabsNavigation = $this.closest('.tabs-navigation');
    const $tabsController = $this.closest('[data-tabs-controller]');
    const firstTabsContent = $tabsController.find('.tabs-content')[0];
    const $tabsContent = $(firstTabsContent);

    const tabName = $this.attr('data-tab');
    const isDisabled = $this.attr('data-tab-disabled');

    if (!tabName || isDisabled) {
      event.preventDefault();
      return;
    }

    event.preventDefault();
    const bodyClasses = $('body').attr('class').split(' ').filter((item) => {
      return !/-tab/.test(item);
    });

    $('body').attr('class', bodyClasses.join(' '));
    $('body').addClass(`-tab-${tabName}`);

    $tabsNavigation.find('.-active').removeClass('-active');
    $this.addClass('-active');

    $tabsContent.children('.-active').removeClass('-active');
    $tabsContent.children(`[data-tab="${tabName}"]`).addClass('-active');

    // Кастомный переключатель табов в меню
    if ($tabs.attr('data-tabs-choiser') !== undefined) {
      // Ищем активный таб и первый активный input
      $tabsContent.find('.choiser.-active input:checked').trigger('click');
    }
  });
}

$(window).resize(() => {
  const $tabs = $('.c-tabs');
  const $allTabsNavbars = $tabs.find('.tabs-navigation');
  const $allTabsContent = $tabs.find('.tabs-content');

  updateFirstTab($allTabsNavbars, $allTabsContent);
});

export default {
  init
};
