'use strict';

import $ from 'jquery';
import _ from './_helpers';

function jumboSlider() {
  const $owlJumbo = $('#jumboSlider');
  $owlJumbo.owlCarousel({
    items: 1,
    navigation: false,
    singleItem: true,
    pagination: false,
    lazyLoad: true
    // autoPlay: true
  });

  $('.jumboslider-controls .-prev').click(() => {
    $owlJumbo.trigger('owl.prev');
  });

  $('.jumboslider-controls .-next').click(() => {
    $owlJumbo.trigger('owl.next');
  });
}

function catalogSlider() {
  const $catalogSlider = $('#catalogSlider');
  $catalogSlider.owlCarousel({
    items: 4,
    navigation: false,
    pagination: false,
    lazyLoad: true,
    addClassActive: true,
    itemsCustom: [
      [0, 2],
      [992, 4]
    ]
    // autoPlay: true
  });

  $('.catalog-slider-controls .-prev').click(() => {
    $catalogSlider.trigger('owl.prev');
  });

  $('.catalog-slider-controls .-next').click(() => {
    $catalogSlider.trigger('owl.next');
  });
}

$(window).resize(() => {
  if (!_.checkWindowWidth(768)) {
    try {
      $('#catalogSlider').data('owlCarousel').destroy();
      return;
    } catch (err) {
      return;
    }
  }

  if (!window.requestAnimationFrame) {
    setTimeout(catalogSlider, 300);
  }

  window.requestAnimationFrame(catalogSlider);
});

function init() {
  jumboSlider();

  if (_.checkWindowWidth(768)) {
    catalogSlider();
  }
}

export default {
  init
};
