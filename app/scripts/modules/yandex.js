'use strict';

import $ from 'jquery';

function maps() {
  YMaps.jQuery(() => {
    $('[id^=ymaps]').each((index, el) => {
      /* eslint no-new: 0 */
      new YMaps.Map(el);
    });
  });
}

export default {
  maps
};
