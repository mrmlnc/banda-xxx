'use strict';

import $ from 'jquery';

// Проверка реальной ширины окна браузера
function checkWindowWidth(value) {
  let e = window;
  let a = 'inner';
  if (!('innerWidth' in window)) {
    a = 'client';
    e = document.documentElement || document.body;
  }

  if (e[a + 'Width'] >= value) {
    return true;
  }

  return false;
}

// Проверка горизонтального ресайза окна
let windowWidth = $(window).width();
function isResizeX() {
  if ($(window).width() !== windowWidth) {
    windowWidth = $(window).width();
    return true;
  }

  return false;
}

let headerBasketBackup;
function drawHeaderBasket(data) {
  const $headerBasket = $('#navbarBasket');
  $headerBasket.find('.basket-item').detach();

  if (!data || !data.products || data.products.length === 0) {
    headerBasketBackup = $headerBasket.children().detach();
    return;
  }

  $headerBasket.append(headerBasketBackup);

  data.products.forEach((product) => {
    const productTemplate = `<div data-remove-id="${product.id}" class="basket-item">
        <div class="item-image"><img src="${product.img}" alt="${product.name}"></div>
          <div class="item-description">
            <div class="title">${product.name}</div>
            <div class="price">${product.price}</div>
          </div>
          <div data-remove class="item-remove">
            <div class="btn"></div>
          </div>
        </div>`;

    const el = $.parseHTML(productTemplate);
    if (el) {
      $(el).prependTo($headerBasket);
    }
  });

  $headerBasket.find('.basket-price .price').html(data.itemsCost);
}

function pizzaShow() {
  const $pizza = $('#loaderBlock');
  return {
    show: () => {
      const $pizzaParts = $pizza.find('.pizza-slice');
      $pizzaParts.addClass('-show');
      $pizza.show();

      let i = 0;
      function nextFrame() {
        if (i < $pizzaParts.length) {
          $($pizzaParts[i]).removeClass('-on');

          i++;
          setTimeout(nextFrame, 500);
        }
      }

      setTimeout(nextFrame, 0);
    },
    hide: () => {
      $pizza.hide();
    }
  };
}

export default {
  checkWindowWidth,
  isResizeX,
  drawHeaderBasket,
  pizzaShow
};
