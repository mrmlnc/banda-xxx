'use strict';

import $ from 'jquery';

function updateFilterTags() {
  const $this = $(this);

  const oldFilter = $this.parent().find('span').html();
  $('#filterForm').find('.label-text').each((index, el) => {
    const $el = $(el);

    if ($el.html() === oldFilter) {
      $el.parent().find('input').prop('checked', false);
      $this.parent().remove();
      return;
    }
  });
}

function updateFilterForm($el) {
  const newFilter = $el.parent().find('.label-text').html();
  const $tags = $('#filterTagsList');

  let status = true;
  $tags.find('span').each((index, tag) => {
    const $tag = $(tag);

    if ($tag.html() === newFilter) {
      $tag.parent().remove();
      status = false;
      return;
    }
  });

  if (status) {
    const $tag = $.parseHTML(`<li class="tag -yellow -close"><span>${newFilter}</span> <a href="#close"></a></li>`);
    $tags.append($tag);
  }
}

function init() {
  const $filterForm = $('#filterForm');
  if (!$filterForm) {
    return;
  }

  const $filterTagsList = $('#filterTagsList');

  $filterTagsList.find('a[href="#close"]').on('click', updateFilterTags);

  $filterForm.find('input').on('change', function() {
    const $this = $(this);

    $.ajax(window.baseUrl + '/basket/add').done((result) => {
      if (result.status === 'ok') {
        updateFilterForm($this);
        $filterTagsList.find('a[href="#close"]').on('click', updateFilterTags);
      }
    });
  });

  $('.filter-reset').on('click', () => {
    $.ajax(window.baseUrl + '/basket/add').done((result) => {
      if (result.status === 'ok') {
        $filterTagsList.empty();
        $filterForm.find('input').prop('checked', false);
      }
    });
  });
}

export default {
  init
};
