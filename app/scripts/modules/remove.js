'use strict';

import $ from 'jquery';
// import _ from './_helpers';

function removeClickHandler(event) {
  const $target = $(event.target);
  const $block = $target.closest('[data-remove-id]');
  const productId = parseInt($block.attr('data-remove-id'), 10);

  // AJAX to remove
  // AjaxToRemove(itemId).then(() => $block.remove());)
  $.ajax({
    url: window.baseUrl + '/basket/delete',
    type: 'GET',
    data: {
      productId,
      _csrf: $('meta[name="csrf-token"]').attr('content'),
      country: null
    }
  }).done((result) => {
    if (result.status === 'ok') {
      const $headerBasket = $block.closest('#navbarBasket');

      $block.remove();

      if ($headerBasket.find('.basket-item').length === 0) {
        $headerBasket.detach();
      }
    }
  });
}

function init() {
  $(document).on('click', '[data-remove]', removeClickHandler);
}

export default {
  init
};
