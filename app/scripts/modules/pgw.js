'use strict';

import $ from 'jquery';
import _ from './_helpers';

$(window).resize(function() {
  // Перерисовка слайдера только при горизонтальном ресайзе
  if (!_.isResizeX()) {
    return;
  }

  if (!window.requestAnimationFrame) {
    setTimeout(pgwInit, 300);
  }

  window.requestAnimationFrame(pgwInit);
});

function pgwInit() {
  const $pgw = $('#pgwSlider');
  if (!$pgw[0]) {
    return;
  }

  // pgw.destroy() отрабатывает не правильно, поэтому после изменения размера
  // экрана, но перед перезагрузкой слайдера - удаляется предыдущий экземпляр
  $('.pgw-slider .ps-current').remove();
  $pgw.pgwSlider({
    mainClassName: 'pgw-slider',
    autoSlide: false,
    displayList: false,
    displayControls: true
  });

  if (_.checkWindowWidth(768)) {
    $pgw.reload({
      mainClassName: 'pgw-slider',
      autoSlide: false
    });
  }

  if (_.checkWindowWidth(1200)) {
    $pgw.reload({
      mainClassName: 'pgw-slider',
      autoSlide: false,
      maxHeight: 320
    });
  }
}

function init() {
  pgwInit();
}

export default {
  init
};
