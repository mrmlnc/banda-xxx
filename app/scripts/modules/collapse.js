'use strict';

import $ from 'jquery';

function hideByContainer(hideEl) {
  const containerName = hideEl.replace('all:', '');

  $(containerName).children().addClass('-hidden').removeClass('-on');
}

function hideByList(hideEl) {
  const hidelArr = hideEl.split(',');
  if (hidelArr.length > 0) {
    hidelArr.forEach((el) => {
      const $el = $(el);
      if (!$el.hasClass('-hidden')) {
        $el.addClass('-hidden');
      }

      $el.removeClass('-on');
    });
  } else {
    const $el = $(hideEl);
    if (!$el.hasClass('-hidden')) {
      $el.addClass('-hidden');
    }

    $el.removeClass('-on');
  }
}

function collapseClickHandler(event) {
  const $triggerEl = $(event.target).closest('[data-collapse]');
  const $targetEl = $($triggerEl.attr('data-target'));

  if ($triggerEl.attr('data-no-scroll') !== undefined) {
    $('html').toggleClass('-scroll-off');
  }

  if ($triggerEl.attr('data-scroll-top') !== undefined) {
    $('body').scrollTop(0);
  }

  const hiddenClass = $triggerEl.attr('data-hidden-class');
  if (hiddenClass !== undefined) {
    $targetEl.find(hiddenClass).toggleClass('-hidden');
  }

  const hideEl = $triggerEl.attr('data-hide');
  if (hideEl !== undefined) {
    // Скрыть все дочерние элементы указанного родителя
    if (hideEl.indexOf('all:') === -1) {
      hideByList(hideEl);
    } else {
      hideByContainer(hideEl);
    }
  }

  $triggerEl.toggleClass('-active');

  const onlyAdd = $triggerEl.attr('data-only-add');
  if (onlyAdd === undefined) {
    $targetEl.toggleClass('-on');
  } else {
    $targetEl.addClass('-on');
  }

  if ($triggerEl.attr('data-no-prevent') === undefined) {
    event.preventDefault();
  }
}

function init() {
  $(document).on('click', '[data-collapse]', collapseClickHandler);
}

export default {
  init
};
