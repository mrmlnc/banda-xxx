'use strict';

import slider from './modules/slider';
import menu from './modules/menu';
import collapse from './modules/collapse';
import pgw from './modules/pgw';
import scroll from './modules/scroll';
import tabs from './modules/tabs';
import remove from './modules/remove';
import cart from './modules/cart';
// import yandex from './modules/yandex';
import awesomeplete from './modules/awesomeplete';
import buy from './modules/buy';
import catalogFilter from './modules/catalog-filter';
import formSender from './modules/form-sender';
import product from './modules/product';

window.baseUrl = document.querySelector('html').getAttribute('data-url');

slider.init();
menu.init();
collapse.init();
pgw.init();
scroll.init();
remove.init();
cart.init();
tabs.init();
// yandex.maps();
awesomeplete.init();
buy.init();
catalogFilter.init();
formSender.init();
product.init();
